import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TodoItem from './TodoItem';

export default class Todos extends Component {
    render() {
        return this.props.todos.map((todo) => {
            return <TodoItem removeBtn={this.props.removeBtn} key={todo.id} todo={todo}
                             markComplete={this.props.markComplete}/>
        });
    }
}
//PropTypes
Todos.propTypes = {
    todos: PropTypes.array.isRequired
};