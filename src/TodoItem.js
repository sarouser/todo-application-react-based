import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class TodoItem extends Component {
    getStyle = () => {
        return {
            boxSizing: "border-box",
            width: "50%",
            borderColor: "#000000",
            float: "left",
            display: "inline block",
            backgroundColor: '#f4f4f4',
            // borderRightColor: (this.props.id % 2 === 0) ? "#6af421" : "rgba(0,0,0,0.79)",
            border: '1px #ccc dotted',
            textDecoration: (this.props.todo.completed) ? 'line-through' : 'none'
        }
    };

    render() {
        const {id, title, completed} = this.props.todo;
        return (
            <div style={this.getStyle()}>
                <p>
                    <input id={id} onChange={this.props.markComplete.bind(this, id)} type="checkbox"/> {' '}
                    {title}
                    <button onClick={this.props.removeBtn.bind(this, id)} style={btnStyle}>X</button>
                </p>
            </div>
        );
    }
}

const btnStyle = {
    cursor: 'pointer',
    backgroundColor: '#fff',
    color: 'red',
    border: 'none',
    borderRadius: '50%'
};

//PropTypes
TodoItem.propTypes = {
    todo: PropTypes.object.isRequired
};