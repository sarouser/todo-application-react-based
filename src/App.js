import React, {Component} from 'react';
import Header from './Header';
import Todos from './Todos';
import './App.css';
import AddTodo from './AddTodo';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Aboutt from "./pages/About";
import axios from 'axios';

export default class App extends Component {
    state = {
        deletedId: undefined,
        todos: []
    };

    componentDidMount() {
        axios.get("https://jsonplaceholder.typicode.com/todos?_limit=10").then(res => {
            // console.log(res.data);
            this.setState({todos: res.data})
        })
    }

    markComplete = (id) => {
        this.setState({
            todos: this.state.todos.map((todo) => {
                if (todo.id === id) {
                    todo.completed = (!todo.completed);
                }
                return todo;
            })
        })
    };
    removeBtn = (id) => {
        this.setState({deletedId: id, todos: [...this.state.todos.filter(todo => todo.id !== id)]});
    };
    addTodoItem = (e) => {
        if (e) {
            let arr = [...this.state.todos];
            console.log(this.state.deletedId);
            arr.unshift({
                id: (!this.state.deletedId ? this.state.todos.length + 1 : this.state.deletedId),
                title: e,
                completed: false
            });
            this.setState({
                deletedId: (arr[0].id === this.state.deletedId) ? undefined : this.state.deletedId,
                todos: arr
            });
        } else alert("Write ToDo before submitting it...");
    };

    render() {
        return (
            <Router>
                <div className="App">
                    <div className="container">
                        <Header/>
                        <Route exact path="/" render={props => (
                            <React.Fragment>
                                <AddTodo addTodoItem={this.addTodoItem}/>
                                <Todos removeBtn={this.removeBtn} markComplete={this.markComplete}
                                       todos={this.state.todos}/>
                            </React.Fragment>
                        )}/>
                        <Route path="/about" component={Aboutt}/>
                    </div>
                </div>
            </Router>

        );
    }
}
