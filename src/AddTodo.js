import React, {Component} from 'react';

export default class AddTodo extends Component {
    state = {
        title: ''
    };

    onChangeFunction = (e) => {
        this.setState({[e.target.name]: e.target.value});
        console.log(this.state);
        console.log(e.target.value);
    };

    addTodoItem = (e) => {
        e.preventDefault();
        this.props.addTodoItem(this.state.title);
        this.setState({title: ''});
    };

    render() {
        return (
            <form
                style={{display: 'flex'}}
                onSubmit={this.addTodoItem}
            >
                <input
                    type="text"
                    name="title"
                    placeholder="Add Todo..."
                    style={{
                        border: "0px",
                        width: "50%"
                    }}
                    value={this.state.title}
                    onChange={this.onChangeFunction}
                >
                </input>
                <input
                    type="submit"
                    value="submit"
                    className="btn"
                    style={{
                        border: "0px",
                        width: "50%",
                        backgroundColor: "#0f232a",
                        color: "white",
                        cursor: "pointer"
                    }}
                >
                </input>
            </form>

        );
    }
}